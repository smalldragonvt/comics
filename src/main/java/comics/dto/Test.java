package comics.dto;

public class Test {

    private String x;
    private int y;
    private SubTest subTest;

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public SubTest getSubTest() {
        return subTest;
    }

    public void setSubTest(SubTest subTest) {
        this.subTest = subTest;
    }

    public static class SubTest {
        private int z;

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }
    }
}
