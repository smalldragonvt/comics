package comics.model;

import comics.dto.Test;
import org.springframework.stereotype.Service;

@Service
//@Scope(scopeName = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TestGenerator {

    public Test generate(String url) {
        Test test = new Test();
        test.setX(url);
        test.setY(27);
        test.setSubTest(new Test.SubTest());
        return test;
    }
}
