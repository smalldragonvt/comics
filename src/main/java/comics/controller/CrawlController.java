package comics.controller;

import comics.dto.Test;
import comics.model.TestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CrawlController {

    private final TestGenerator testGenerator;

    @Autowired
    public CrawlController(TestGenerator testGenerator) {
        this.testGenerator = testGenerator;
    }

    @RequestMapping(name = "/crawl", produces = "application/json")
    public Test test(@RequestParam(name = "url") String url) {
        return testGenerator.generate(url);
    }
}
